/*
* HieuHT24
*/
package day5;

public class Day5 {
	public static void main(String[] args) {
//		boolean check = false;
//		int a = 10;
//		String b = "ITF02";
//		String c = "ITF02";
//		String d = new String("itf02");
//
//		if (b == d) {
//			System.out.println("Kiểm tra if else");
//		} else {
//			System.out.println("Kết quả còn lại");
//		}
//		// equalsIgnoreCase là so sánh giá trị không phân biệt
//		// chữ hoa hay thường
//		if (b.equalsIgnoreCase(d)) {
//			System.out.println("Kiểm tra if else");
//		} else {
//			System.out.println("Kết quả còn lại");
//		}
//
//		if (b.equals(d))
//			System.out.println("Kiểm tra if else");
//		else
//			System.out.println("Kết quả còn lại");
//
//		if (a == 10) {
//			if (b.equals(d))
//				System.out.println("Kiểm tra if else");
//			else
//				System.out.println("Kết quả còn lại");
//		} else if (a < 10) {
//			if (b.equals(d))
//				System.out.println("Kiểm tra if else");
//			else
//				System.out.println("Kết quả còn lại");
//		} else {
//			if (b.equals(d))
//				System.out.println("Kiểm tra if else");
//			else
//				System.out.println("Kết quả còn lại");
//		}
		
		String gender = "male";
		int age = 30;
		
		if(gender == "male" && age <20) {
			System.out.println("Boy");
			return;
		}
		
		if(gender == "male" && age >= 20) {
			System.out.println("Man");
			return;
		}
		
		if(gender == "female" && age < 20) {
			System.out.println("Girl");
			return;
		}
		
		if(gender == "female" && age >= 20) {
			System.out.println("Woman");
			return;
		}
	}
}
