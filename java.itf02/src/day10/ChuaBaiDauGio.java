/*
* HieuHT24
*/
package day10;

import java.util.Scanner;

public class ChuaBaiDauGio {
//	Nhập bàn phím mảng 2 chiều m x n
//	1- Tính tổng từng hàng
//	2- tính trung bình cộng từng hàng
//	3- tìm phần tử lớn nhất của mảng 2 chiều
	static int soHang;
	static int soCot;
	static int[][] mangHaiChieu = new int[soHang][];
	static Scanner sc = new Scanner(System.in);

	public static void nhapMangHaiChieu() {
		System.out.println("Nhập số hàng: ");
		soHang = sc.nextInt();
		sc.nextLine();

		while (soHang <= 0) {
			System.out.println("Nhập lại số hàng: ");
			soHang = sc.nextInt();
			sc.nextLine();
		}
		mangHaiChieu = new int[soHang][];

		for (int i = 0; i < mangHaiChieu.length; i++) {
			System.out.println("Nhập số cột ở hàng thứ " + i);
			soCot = sc.nextInt();
			sc.nextLine();
			mangHaiChieu[i] = new int[soCot];
			for (int j = 0; j < mangHaiChieu[i].length; j++) {
				System.out.println("Nhập phần tử lại vị trí [" + i + "," + j + "]");
				mangHaiChieu[i][j] = sc.nextInt();
				sc.nextLine();
			}
		}
	}

	public static void tinhToanTrongMang() {
		for (int i = 0; i < mangHaiChieu.length; i++) {
			int tongTungHang = 0;
			for (int j = 0; j < mangHaiChieu[i].length; j++) {
				tongTungHang += mangHaiChieu[i][j];
			}
			System.out.println("Tổng hàng thứ " + i + " là " + tongTungHang);
			System.out.println("Trung bình cộng hàng thứ " + i + " là " + (tongTungHang / mangHaiChieu[i].length));
			System.out.println();
		}
	}

	public static void soLonNhatTrongMang() {
		int soLonNhatTrongMang = mangHaiChieu[0][0];
		for (int i = 0; i < mangHaiChieu.length; i++) {
			for (int j = 0; j < mangHaiChieu[i].length; j++) {
				if (mangHaiChieu[i][j] > soLonNhatTrongMang) {
					soLonNhatTrongMang = mangHaiChieu[i][j];
				}
			}
		}
		System.out.println("Số lớn nhất trong mảng là " + soLonNhatTrongMang);
	}

	public static void hienThiMangHaiChieu() {
		System.out.println("Mảng hai chiều: ");
		for (int i = 0; i < mangHaiChieu.length; i++) {
			for (int j = 0; j < mangHaiChieu[i].length; j++) {
				System.out.print(mangHaiChieu[i][j] + " ");
			}
			System.out.println();
		}
	}

	public static void main(String[] args) {

//		while (true) {
//			System.out.println("Quản lý mảng: ");
//			System.out.println("1. Nhập mảng");
//			System.out.println("2. Tính tổng và trung bình cộng từng hàng");
//			System.out.println("3. Tìm số lớn nhất trong mảng");
//			System.out.println("4. Hiển thị mảng");
//			System.out.println("0. Kết thúc chương trình");
//
//			System.out.println("Mời bạn nhập lựa chọn: ");
//			int chon;
//			chon = sc.nextInt();
//			sc.nextLine();
//			while (mangHaiChieu.length == 0 && chon != 1) {
//				System.out.println("Mời lựa chọn số 1 để nhập mảng trước vì mảng đang trống: ");
//				chon = sc.nextInt();
//			}
//
//			switch (chon) {
//			case 1:
//				nhapMangHaiChieu();
//				break;
//			case 2:
//				tinhToanTrongMang();
//				break;
//			case 3:
//				soLonNhatTrongMang();
//				break;
//			case 4:
//				hienThiMangHaiChieu();
//				break;
//			case 0:
//				System.out.println("Kết thúc chương trình");
//				System.exit(0);
//				break;
//			default:
//				break;
//			}
//		}
		String tiepTuc;
		do {
			System.out.println("Quản lý mảng: ");
			System.out.println("1. Nhập mảng");
			System.out.println("2. Tính tổng và trung bình cộng từng hàng");
			System.out.println("3. Tìm số lớn nhất trong mảng");
			System.out.println("4. Hiển thị mảng");
			System.out.println("0. Kết thúc chương trình");

			System.out.println("Mời bạn nhập lựa chọn: ");
			int chon;
			chon = sc.nextInt();
			sc.nextLine();
			while (mangHaiChieu.length == 0 && chon != 1) {
				System.out.println("Mời lựa chọn số 1 để nhập mảng trước vì mảng đang trống: ");
				chon = sc.nextInt();
			}

			switch (chon) {
			case 1:
				nhapMangHaiChieu();
				break;
			case 2:
				tinhToanTrongMang();
				break;
			case 3:
				soLonNhatTrongMang();
				break;
			case 4:
				hienThiMangHaiChieu();
				break;
			case 0:
				System.out.println("Kết thúc chương trình");
				System.exit(0);
				break;
			default:
				break;
			}
			
			System.out.println("Bạn có muốn tiếp tục hay không? (Y/N)");
			tiepTuc = sc.nextLine();
			
		} while (tiepTuc.equalsIgnoreCase("Y"));
		System.out.println("Kết thúc chương trình!");
	}
}
