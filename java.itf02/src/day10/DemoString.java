/*
* HieuHT24
*/
package day10;

public class DemoString {
	public static void main(String[] args) {
		// Cách 1 khởi tạo biến String nhưng không gán giá trị
		String stg1;
		stg1 = "ITF02";

		// Cách 2: khởi tạo biến String và gán giá trị luôn
		String stg2 = "ITF02";

		// Cách 3: khởi tạo biến String như một object
		String stg3 = new String("ITF02");

		System.out.println(stg1 == stg2);
		System.out.println(stg1 == stg3);
		// So sánh == thì là so sánh về tham chiếu và tham trị

		System.out.println(stg1.equals(stg3));
		// equals là so sánh về tham trị và phân biệt chữ hoa chữ thường
		System.out.println(stg1.equalsIgnoreCase(stg3));
		// equalsIgnoreCase là so sánh tham trị không quan tâm hoa hay thường
		
		//so sáng next() và nextLine()
		System.out.println(stg1 + " " + stg2);
		System.out.println(stg1.concat(stg3));
		System.out.println(1 + "1");
		System.out.println("1" + 1 + 1);
		String stg4 = "Lớp \"ITF02\" học rất vui rất vui";
		System.out.println(stg4);
		String url = "D:\\HieuHT24\\Document\\Fsoft-Training-with-Fee\\FTF_ITFH_v1.1\\FTF_ITFH_v1.1\\Java\\1_Lectures";
		System.out.println(url);
		System.out.println(url.length());
		System.out.println(url.charAt(0));
		System.out.println(url.charAt(87));
		System.out.println(stg4.indexOf('a'));
		System.out.println(stg4.indexOf("rất"));
		System.out.println(stg4.indexOf("rất", 17));
		System.out.println(stg4.substring(5));
		//[5,9) >= && < 9
		System.out.println(stg4.substring(5, 9));
		System.out.println(stg4.toLowerCase());
		System.out.println(stg4.toLowerCase());
		System.out.println(stg4.startsWith("L"));
		System.out.println(stg4.endsWith("I"));
		
		//erros exception
		System.out.println(stg4.contains("itf"));
		System.out.println(stg4.replace(" ", ""));
		String userName = "HieuHT24 ";
		String pass = "1234 ";
		System.out.println(userName.trim());
	}
}
