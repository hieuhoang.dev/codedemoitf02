/*
* HieuHT24
*/
package day8;

import java.util.Scanner;

public class LuyenTapMang {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Nhập số lượng phần tử: ");
		int n = sc.nextInt();
		sc.nextLine();
		
		int[] array = new int[n];
		for (int i = 0; i < array.length; i++) {
			System.out.println("Nhập phần tử lại vị trí thứ: " + i);
			array[i] = sc.nextInt();
			sc.nextLine();
		}
		
		System.out.println("Mảng vừa nhập là: ");
		for (int i = 0; i < array.length; i++) {
			System.out.print(array[i] + " ");
		}
		System.out.println();
		
		int max = array[0];
		int min = array[0];
		
		for (int a : array) {
			if(a > max) {
				max = a;
			}
			
			if(a < min) {
				min = a;
			}
		}
		
		System.out.println("Giá trị lớn nhất: " + max);
		System.out.println("Giá trị nhỏ nhất: " + min);
		
		//Cách 2 là sắp xếp mảng trước
		int[] newArray = array;
		for (int i = 0; i < newArray.length; i++) {
			for (int j = i + 1; j < newArray.length; j++) {
				
				if(array[i] > array[j]) {
					int temp = array[j];
					array[j] = array[i];
					array[i] = temp;
				}
			}
		}
//		System.out.println("Mảng sau sắp xếp: ");
//		for (int i = 0; i < newArray.length; i++) {
//			System.out.print(newArray[i] + " ");
//		}
//		System.out.println();
//		System.out.println("Giá trị lớn nhất sau sắp xếp: " + newArray[n - 1]);
//		System.out.println("Giá trị nhỏ nhất sau sắp xếp: " + newArray[0]);
		
		//Câu 4
		//khởi tạo một mảng để lưu số lần lặp lại
		int[] soLanLapLai = new int[n];
		//khởi tạo biến đếm
		int count = 0;
		
		//khởi tạo một mảng lưu trữ các số đã tìm kiếm
		int[] danhSachSoDaTimKiem = new int[n];
		
		//Khởi tạo biến để kiểm tra xem số đã được tìm kiếm chưa
		boolean check = false;
		
		// dùng vòng lặp for để kiểm tra giá trị
		for (int i = 0; i < array.length; i++) {
			//Mỗi lần kiểm tra thì khởi tạo lại các biến
			count = 0;
			check = false;
			//Vòng lặp để kiểm tra trùng lặp
			for (int j = 0; j < danhSachSoDaTimKiem.length; j++) {
				if(array[i] == danhSachSoDaTimKiem[j]) {
					check = true;
				}
			} 
			
			//Nếu đã trùng lặp thì nhảy sang số khác
			if(check == true) {
				continue;
			}
			
			//Vòng lặp để đếm xem số đó xuất hiện bao nhiêu lần
			for (int j = 0; j < array.length; j++) {
				if(array[i] == array[j]) {
					count++;
				}
			}
			
			//thêm số kiểm tra vào mảnh đã tìm kiếm
			danhSachSoDaTimKiem[i] = array[i];
			//thêm số lần đã tìm kiếm vào mảng
			soLanLapLai[i] = count;
		}
		//kiểm tra mảng số lần lặp lại có đúng không
		System.out.println("Mảng số lần lặp lại: ");
		for (int i = 0; i < soLanLapLai.length; i++) {
			System.out.print(soLanLapLai[i] + " ");
		}
		System.out.println();
		//Kiểm tra các đã đã tìm kiếm
		System.out.println("Mảng lưu trữ các số đã tìm kiếm: ");
		for (int i = 0; i < danhSachSoDaTimKiem.length; i++) {
			System.out.print(danhSachSoDaTimKiem[i] + " ");
		}
		
		//Tìm ra số lần tìm kiếm nhiều nhất
		int soLanTimKiemLonNhat = 0;
		for (int i : soLanLapLai) {
			if(i > soLanTimKiemLonNhat) {
				soLanTimKiemLonNhat = i;
			}
		}
		
		//In ra số và số lần xuất hiện nhiều nhất
		System.out.println();
		for (int i = 0; i < soLanLapLai.length; i++) {
			if(soLanLapLai[i] == soLanTimKiemLonNhat) {
				System.out.println("Số " + danhSachSoDaTimKiem[i] + " xuất hiện " + soLanLapLai[i]);
			}
		}
	}
}
