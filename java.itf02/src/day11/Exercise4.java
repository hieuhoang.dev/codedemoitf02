/*
* HieuHT24
*/
package day11;

import java.util.Scanner;

public class Exercise4 {
//	public static void main(String[] args) {
//		String chuoi = "Fresher Academy";
//		
//		char[] mangKyTu = chuoi.toCharArray();
//		
////		char[] mangKyTu = new char[chuoi.length()];
////		for (int i = 0; i < mangKyTu.length; i++) {
////			mangKyTu[i] = chuoi.charAt(i);
////		}
//		
//		//Mảng này để lưu giá trị số lần lặp lại
//		int[] soLanLapLai = new int[chuoi.length()];
//		
//		//Lưu các ký tự đã kiểm tra
//		char[] kyTuDaKiemTra = new char[chuoi.length()];
//		
//		//Mục đích để xem xuất hiện bao nhiêu lần
//		int dem;
//		
//		//Mục đích là để xem đã kiểm tra hay chưa
//		boolean kiemTra;
//		for (int i = 0; i < mangKyTu.length; i++) {
//			dem = 0;
//			kiemTra = false;
//			//Mục đích để kiểm tra xem ký tự đã tìm kiếm hay chưa
//			for (int j = 0; j < kyTuDaKiemTra.length; j++) {
//				//Nếu mà cái ký tự đã kiểm tra rồi thì biến kiemTra = true
//				if(mangKyTu[i] == kyTuDaKiemTra[j]) {
//					kiemTra = true;
//				}
//			}
//			
//			//Nếu ký đã được kiểm tra thì bỏ qua ký tự đó
//			//không đếm số lần lặp lại nữa
//			if(kiemTra == true) {
//				//kyTuDaKiemTra[i] = ' ';
//				continue;
//			}
//			
//			//Kiểm tra xem ký tự lặp lại  bao nhiêu lần
//			for (int j = 0; j < mangKyTu.length; j++) {
//				if(mangKyTu[i] == mangKyTu[j]) {
//					dem++;
//				}
//			}
//			
//			//Nạp số lần lặp lại vào vị trí tương ứng trong mảng
//			//vì số lượng phần tử và các vị trí tương đồng nhau
//			soLanLapLai[i] = dem;
//			
//			//Nạp ký tự đã tìm kiếm và mảng
//			kyTuDaKiemTra[i] = mangKyTu[i];
//		}
//		
//		//Kiểm tra xem ký tự lặp lại 2 lần
//		String chuoiDaLoc = "";
//		for (int i = 0; i < kyTuDaKiemTra.length; i++) {
//			if(kyTuDaKiemTra[i] != '\u0000') {
//				chuoiDaLoc += kyTuDaKiemTra[i];
//			}
//			System.out.print(soLanLapLai[i] + " ");
//			//chuoiDaLoc += kyTuDaKiemTra[i];
//		}
//		System.out.println();
//		char ch = '\u0000';
//		System.out.println(chuoiDaLoc);
//		System.out.println(chuoi.replace(" ", ""));
//	}
	
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);

		System.out.println("Give me a string: ");
		String string1 = sc.nextLine();
		char[] character = string1.toCharArray();

		for (char c : character) {
			System.out.print(c + " ");
		}

		int count = 0;
		System.out.println();
		for (int i = 0; i < character.length; i++) {
			count = 0;
			for (int j = 0; j < i; j++) {
				if (character[i] != character[j]) {
					count++;
				}
			}
			if (count == i) {
				System.out.print(character[i]);
			}

		}

		sc.close();
	}
}
