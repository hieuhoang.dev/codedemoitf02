/*
* HieuHT24
*/
package day11;

public class Exercise1 {
	public static void main(String[] args) {
		String chuoiBanDau = "Fresher Academy";
		String chuoiSoSanh = "FRESHER ACADEMY";
		
		if(chuoiBanDau.equalsIgnoreCase(chuoiSoSanh)) {
			System.out.println("\"" + chuoiBanDau + "\" is equal to \"" + chuoiSoSanh + "\"" );
		}
	}
}
