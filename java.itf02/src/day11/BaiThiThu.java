/*
* HieuHT24
*/
package day11;

import java.util.Iterator;
import java.util.Scanner;

public class BaiThiThu {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Nhập chuỗi: ");
		String chuoi;
		chuoi = sc.nextLine();
		while (chuoi.length() <= 0 || chuoi.length() > 100) {
			System.out.println("Nhập lại chuỗi: ");
			chuoi = sc.nextLine();
		}
		
		System.out.println("Nhập ký tự cần tìm kiếm: ");
		char kyTu = sc.next().charAt(0);
		
		int ketQua = chuoi.indexOf(kyTu);
		boolean kiemTra = chuoi.contains(String.valueOf(kyTu));
//		System.out.println(ketQua);
//		System.out.println(kiemTra);
		
		//Cách 1: biến cái chuỗi của chúng ra thành một mảng ký tự
		if(ketQua >= 0) {
			char[] mangKyTu = chuoi.toCharArray();
			//
			System.out.println("Chuỗi đã đảo ngược: ");
			for (int i = mangKyTu.length - 1; i >= 0 ; i--) {
				//System.out.println(i);
				System.out.print(mangKyTu[i]);
			}
		}
		System.out.println();
		//Cách 2 sử dụng hàm charAt() 
		//và khởi tạo một biến chuỗi mới để lưu chuỗi đảo ngược
		if(kiemTra == true) {
			String chuoiDaoNguoc = "";
			for (int i = 0; i < chuoi.length(); i++) {
				//a+=b => a = a + b;
				chuoiDaoNguoc =  chuoi.charAt(i) + chuoiDaoNguoc;
			}
			System.out.println("Chuỗi đã đảo ngược: " + chuoiDaoNguoc);
		}
		
		//Cách 3: sử dụng StringBuilder
		if(kiemTra == true) {
			StringBuilder chuoiDao = new StringBuilder(chuoi);
			System.out.println("Chuỗi đã đảo ngược: " + chuoiDao.reverse());
		}
	}
}
