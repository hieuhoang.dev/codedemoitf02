/*
* HieuHT24
*/
package day2;

public class DemoDay2 {
	public static int bienToanCuc = 10;
	
	//hai loại hàm(method, function)
	//phạm vi truy cập: 
	//public là có gọi đến được trong cùng 1 project
	//private là chỉ có thể truy cập trong 1 class
	//loại 1: phạm vi truy cập void tên hàm thì không cần return
	//loại 2: phạm vi truy cập kiểu dữ liệu(vd float, int, .. ) 
	//thì bắt buộc phải có return trả về giá trị tương ứng
	
	//từ khóa static có nghĩa là có thể sử dụng 
	//không cần khởi tạo đối tượng
	
	//nếu hàm chúng ta viết ra mà không có từ khóa static
	//thì biến toàn cục cũng không cần static vẫn có thể sử dụng trực tiếp
	public static void ham1() {
		bienToanCuc = 20;
		System.out.println(bienToanCuc);
	}
	
	public static int ham2() {
		return 9;
	}
	
	public static void main(String[] args) {
		//Cách 1: khởi tạo một đối tượng
		//Class là xây dựng một khuôn mẫu trong đó chưa thông tin
		//và hành động
		//blueprint
		//Ví dụ xây dựng một class mô tả học viên lớp ITF02
		//Object(Đối tượng) là triển khai class đã xây dựng
		//Tên class tên đối tượng = new Tên class
//		DemoDay2 doiTuong = new DemoDay2();
//		doiTuong.bienToanCuc = 40;
		
		//Cách 2 khi khai báo biến thì thêm từ khóa static
		//thì chúng ta có thể gọi mà không cần khởi tạo đối tượng
		bienToanCuc = 40;
		System.out.println(bienToanCuc);
		ham1();
		System.out.println(ham2());
		
		
		//Biến cục bộ là biến chỉ có giá trị trong một hàm
		//Cú pháp báo
		//kiểu dữ liệu tên biến = (gán giá trị) 20;
		int soNguyen = 6;
		char kyTu = 'a';
		String chuoi = "Lớp ITF02 rất vui";
		boolean kiemTra = false;
		
		//System.out.println(kiemTra);
		
		byte a = 127;
		byte b = 123;
		short c = (short) (a + b);
		
		float d = 20;
		float e = 20f;
		float f = 20F;
		
		int g;
		g = 10;
		System.out.println(Math.PI);
	}
}
