/*
* HieuHT24
*/
package day2;

public class StaticAndNoStatic {
	public int a = 10;
	public static int b = 20;
	public static final int D = 30; 
//	Hằng số là bất biến
	
	public void noStatic() {
		System.out.println(a);
		System.out.println(b);
	}
	
	public static void main(String[] args) {
		StaticAndNoStatic tenDoiTuong = new StaticAndNoStatic();
		int c = tenDoiTuong.a + b + D;
		System.out.println(tenDoiTuong.a);
		System.out.println(b);
		System.out.println(c);
		System.out.println("--------------");
//		tenDoiTuong.noStatic();
		
		//D = 40;
	}
}
