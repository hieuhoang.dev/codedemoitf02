/*
* HieuHT24
*/
package day6;

import java.util.Scanner;

public class BaiTapDauGio {
	static Scanner sc = new Scanner(System.in);
	static Student student = new Student();
	// static Student[] s = new Student[10];

	public static void inputStudent() {
		System.out.println("Nhập thông tin sinh viên");
		System.out.println("Nhập tên: ");
		student.name = sc.nextLine();
		System.out.println("Nhập tuổi: ");
		student.age = sc.nextInt();
		sc.nextLine();
		System.out.println("Nhập địa chỉ: ");
		student.address = sc.nextLine();
		System.out.println("Nhập điểm toán: ");
		student.mathScore = sc.nextFloat();
		System.out.println("Nhập điểm văn: ");
		student.literatureScore = sc.nextFloat();
		System.out.println("Nhập điểm anh: ");
		student.englistScore = sc.nextFloat();
		System.out.println("Nhập điểm sinh: ");
		student.biologicalScore = sc.nextFloat();
		System.out.println("Nhập điểm sử: ");
		student.historyScore = sc.nextFloat();
		System.out.println("Nhập điểm địa: ");
		student.geographicalScore = sc.nextFloat();
		sc.nextLine();
		System.out.println("Bạn có muốn nhập lại thông tin sinh viên hay không? (Y/N)");
		String nhapLai = sc.nextLine(); //Y y
		if(nhapLai.equalsIgnoreCase("y")) {
			inputStudent();
		}
	}

	public static void displayStudent() {
		System.out.println("Thông tin sinh viên: ");
		System.out.println(student.name + " | " + student.age + " | " + student.address + " | " + student.mathScore
				+ " | " + student.literatureScore + " | " + student.englistScore + " | " + student.biologicalScore
				+ " | " + student.historyScore + " | " + student.geographicalScore);
	}

	public static void averageScore() {
		float average = (student.mathScore + student.literatureScore + student.englistScore + student.biologicalScore
				+ student.historyScore + student.geographicalScore) / 6;
		System.out.println("Điểm trung bình của sinh viên: " + average);
	}

	public static void main(String[] args) {
//		inputStudent();
//		displayStudent();
//		averageScore();
		while (true) {
			System.out.println("Hệ thống quản lý thông tin sinh viên");
			System.out.println("1. Nhập thông tin sinh viên");
			System.out.println("2. Hiển thị thông tin sinh viên");
			System.out.println("3. Tính điểm trung bình của sinh viên");
			System.out.println("0. Thoát chương trình");
			System.out.println("Mời bạn nhập lựa chọn: ");
			int chon = sc.nextInt();
			sc.nextLine();

			switch (chon) {
			case 1:
				inputStudent();
				break;
			case 2:
				displayStudent();
				break;
			case 3:
				averageScore();
				break;
			case 0:
				System.out.println("Kết thúc chương trình!");
				System.exit(0);
				break;
			default:
				System.out.println("Nhập không hợp lệ!");
				break;
			}
			
			//int evenNumber = 10;
		}
	}
}
