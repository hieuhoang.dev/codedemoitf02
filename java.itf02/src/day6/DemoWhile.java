/*
* HieuHT24
*/
package day6;

import java.util.Scanner;

public class DemoWhile {
	public static void main(String[] args) {
//		int a = 10;
//		while (a >= 10) {
//			//a--;
//			System.out.println("Thỏa mãn điều kiện while");
//			break;
//		}
		int chieuDai, chieuRong;
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Nhập chiều dài: ");
		chieuDai = sc.nextInt();
		System.out.println("Nhập chiều rộng: ");
		chieuRong = sc.nextInt();
		
		while (chieuDai < chieuRong) {
			System.out.println("Nhập lại chiều dài: ");
			chieuDai = sc.nextInt();
			System.out.println("Nhập lại chiều rộng: ");
			chieuRong = sc.nextInt();
		}
	}
}
