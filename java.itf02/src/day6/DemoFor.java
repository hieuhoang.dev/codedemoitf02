/*
* HieuHT24
*/
package day6;

public class DemoFor {
	public static void demoReturn(String name) {
//		int a = 10;
//		while (a >= 10) {
//			a++;
//			if(a == 100) {
//				System.out.println(a);
//				break;
//			}
//		}
//		System.out.println("Đã kết thúc");
		System.out.println("Xin chào " + name);
	}
	
	public static void main(String[] args) {
		
//		int sum = 0;
//		for (int i = 1; i <= 10; i++) {
//			sum+=i; //sum = sum + i
//			System.out.println("Quá trình chạy: " + sum);
//		}
//		System.out.println("Kết quả: " + sum);
////		System.out.println("-------------");
////		for (i = 0; i < 10; ++i) {
////			System.out.println(i);
////		}
//		
//		for (int i = 0, j = 10; i != j; i++, j--) {
//			System.out.println(i + " - " + j);
//		}
////		System.out.println(i);
//		//error
//		//exception
		
//		for (int i = 5; i >= 1; i--) {
//			for (int j = 1; j <= i; j++) {
//				System.out.print(j + " ");
//			}
//			System.out.println();
//		}
		
		demoReturn("Trâm");
	
	}
}
