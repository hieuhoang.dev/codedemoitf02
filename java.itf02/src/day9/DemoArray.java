/*
* HieuHT24
*/
package day9;

public class DemoArray {
	public static void main(String[] args) {
		// cho một mảng gồm nhiều số xuất hiện nhiều lần
		// bài toán đặt ra là tìm xe phần tử nào xuất hiện nhiều nhất
		// Có thể có nhiều phần tử xuất hiện số lần giống nhau

		int[] array = { 2, 3, 4, 3, 2, 2, 6, 7, 8, 8, 9, 9, 9 };
		
		//tìm số lần lặp lại của mỗi phần tử nhưng
		//nếu đã kiểm tra thì phải loại bỏ không kiểm tra nữa
		
		int[] danhSachSoLanLapLai = new int[array.length];
		int dem;
		
		int[] danhSachSoDaTimKiem = new int[array.length];
		boolean kiemTra;
		
		for (int i = 0; i < array.length; i++) {
			dem = 0;
			kiemTra = false;
			
			for (int j = 0; j < danhSachSoDaTimKiem.length; j++) {
				if(array[i] == danhSachSoDaTimKiem[j]) {
					kiemTra = true;
				}
			}
			
			if(kiemTra == true) {
				continue;
			}
			
			for (int j = 0; j < array.length; j++) {
				if(array[i] == array[j]) {
					dem++;
				}
			}
			
			danhSachSoDaTimKiem[i] = array[i];
			danhSachSoLanLapLai[i] = dem;
		}
		
		System.out.println("Danh sách số lần đã lặp lại: ");
		for (int i = 0; i < danhSachSoLanLapLai.length; i++) {
			System.out.print(danhSachSoLanLapLai[i] + " ");
		}
		
		System.out.println();
		int soLanLapLaiNhieuNhat = 0;
//		for (int i = 0; i < danhSachSoLanLapLai.length; i++) {
//			if(danhSachSoLanLapLai[i] > soLanLapLaiNhieuNhat) {
//				soLanLapLaiNhieuNhat = danhSachSoLanLapLai[i];
//			}
//		}
		for (int i : danhSachSoLanLapLai) {
			if(i > soLanLapLaiNhieuNhat) {
				soLanLapLaiNhieuNhat = i;
			}
		}
		
		for (int i = 0; i < danhSachSoLanLapLai.length; i++) {
			if(danhSachSoLanLapLai[i] == soLanLapLaiNhieuNhat) {
				System.out.println("Số " + danhSachSoDaTimKiem[i]
								 + " xuất hiện " + danhSachSoLanLapLai[i] 
								 + " lần");
			}
		}
	}
}
