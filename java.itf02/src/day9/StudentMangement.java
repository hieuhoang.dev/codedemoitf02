/*
* HieuHT24
*/
package day9;

import java.util.Scanner;

public class StudentMangement {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Nhập số lượng sinh viên: ");
		int soLuong = sc.nextInt();
		sc.nextLine();
		
		Student[] listStudent = new Student[soLuong];
		
		
		for (int i = 0; i < listStudent.length; i++) {
			Student student = new Student();
			System.out.println("Nhập tên sinh viên thứ " + i);
			student.name = sc.nextLine();
			System.out.println("Nhập tuổi sinh viên thứ " + i);
			student.age = sc.nextInt();
			sc.nextLine();
			System.out.println("Nhập địa chỉ sinh viên thứ " + i);
			student.address = sc.nextLine();
			listStudent[i] = student;
		}
		
		System.out.println("Danh sách sinh viên: ");
		for (int i = 0; i < listStudent.length; i++) {
			System.out.println(listStudent[i].name + " - " 
							 + listStudent[i].age + " - "
							 + listStudent[i].address );
		}
	}
}
