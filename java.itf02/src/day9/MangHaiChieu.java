/*
* HieuHT24
*/
package day9;

import java.util.Scanner;

public class MangHaiChieu {
	public static void main(String[] args) {
//		//Mảng hai chiều hình chữ nhật
//		//Mảng hai chiều được cấu tạo hồm số hàng và số cột
//		//Mảng hai chiều hình chữ nhật nghĩa là xác định được
//		//số hàng số cột từ đầu
//		//int[số hàng][số cột]
//		
//		int[][] array = new int[3][4];
//		
//		array[2][3] = 10;
//		
//		//Mảng hai chiều hình răng cưa
//		//Là loại mảng xác định trước số hàng 
//		//nhưng chưa xác định số cột
//		//Nghĩa là mỗi hàng có thể có số cột khác nhau
//		int[][] array2 = new int[3][];
//		array2[0] = new int[3];
//		array2[1] = new int[2];
//		array2[2] = new int[6];
//		
//		//Để hiển thị các phần tử trong mảng 2 chiều thì 
//		//cần 2 vòng lặp for
//		//vòng lặp for thứ nhất để duyệt theo từng hàng
//		//vòng lặp for thứ hai để duyệt theo từng cột
//		for (int i = 0; i < array.length; i++) {
//			for (int j = 0; j < array[i].length; j++) {
//				System.out.print(array[i][j] + " ");
//			}
//			System.out.println();
//		}
//		
//		System.out.println("------------");
//		for (int i = 0; i < array2.length; i++) {
//			for (int j = 0; j < array2[i].length; j++) {
//				System.out.print(array2[i][j] + " ");
//			}
//			System.out.println();
//		}
		Scanner sc = new Scanner(System.in);
        System.out.println("Nhập số hàng");
        int n = sc.nextInt();
        System.out.println("Nhâp số cột");
        int m = sc.nextInt();
        int[][] array = new int[n][m];
//        for (int index = 0; index < array.length; index++) {
//            for (int i = 0; i < array.length; i++) {
//                System.out.print("phần tử thứ" + index + " " + i);
//                array[index][i] = sc.nextInt();
//
//            }
//
//        }
        for (int index = 0; index < array.length; index++) {
            for (int i = 0; i < array.length; i++) {

                System.out.print(array[index][i] + " ");
            }
            System.out.println();
        }

	}
}
