/*
* HieuHT24
*/
package day7;

import java.util.Scanner;

public class DemoArray {
	public static void main(String[] args) {
//		// Cách 1 thì sẽ xác định được kiểu dữ liệu và tổng số lượng phần tử
//		int[] array = new int[9];
//		String[] stringArray = new String[10];
//
//		// Cách 2 thì sẽ xác định được kiểu dữ liêu,
//		//tổng số lượng phần tử và giá trị từng vị trí của mảng
//		int[] array2 = { 1, 2, 3, 4, 5 };
//		
//		//cách 3 thì không xác định tổng số lượng phần tử và giá trị
//		int[] array3;
//		array3 = new int[9];
//		
//		//cách 1 sẽ hiển thị giá trị dựa vị trí(index) của phần từ trong mảng
//		//trước khi gán giá trị
//		for (int i = 0; i < array.length; i++) {
//			System.out.print(array[i] + " ");
//		}
////		System.out.println();
////		for (int i = 0; i < stringArray.length; i++) {
////			System.out.print(stringArray[i] + " ");
////		}
////		System.out.println();
////		//Cách 2 hiển thị các giá trị dựa theo danh sách giá trị
////		for (int i : array) {
////			System.out.print(i + " ");
////		}
////		System.out.println();
////		for (var i : array2) {
////			System.out.print(i + " ");
////		}
//		
//		//Gán giá trị cho mảng
//		System.out.println();
//		array[0] = 10;
//		array[8] = 11;
//		for (int i = 0; i < array.length; i++) {
//			System.out.print(array[i] + " ");
//		}
		Scanner sc = new Scanner(System.in);
		System.out.println("Nhập số lượng phần tử của mảng: ");
		int n = sc.nextInt();
		sc.nextLine();
		int[] array = new int[n];
		
		for (int i = 0; i < array.length; i++) {
			System.out.println("Nhập giá trị tại vị trí: " + i);
			array[i] = sc.nextInt();
			sc.nextLine();
		}
		
		System.out.println();
		System.out.println("Danh sách mảng: ");
		for (int i = 0; i < array.length; i++) {
			System.out.print(array[i] + " ");
		}
	}
}
