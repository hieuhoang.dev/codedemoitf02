/*
* HieuHT24
*/
package day7;

public class DemoContinue {
	public static void main(String[] args) {
//		for (int i = 0; i < 5; i++) {
//			System.out.print(i + " ");
//		}
		
//		System.out.println();
//		System.out.println("-----------");
//		for (int i = 0; i < 5; i++) {
//			if(i == 3) {
//				return;
//			}
//			System.out.print(i + " ");
//		}
		
//		System.out.println();
//		System.out.println("-----------");
//		for (int i = 0; i < 5; i++) {
//			if(i == 3) {
//				break;
//			}
//			System.out.print(i + " ");
//		}
//		System.out.println();
//		System.out.println("-----------");
		for (int i = 1; i < 10; i++) {
			if(i % 2 == 1) {
				continue;
			}
			System.out.print(i + " ");
		}
	}
}
