/*
* HieuHT24
*/
package day4;

import java.util.Scanner;

public class PhuongTrinhBacHai {

	public static void giaiPhuongTrinhBacHai(double a, double b, double c) {
		double delta = b * b - 4 * a * c;

		if (delta < 0) {
			System.out.println("Phương trình vô nghiệm");
			// Trong trường hợp này return là để kết thúc thực thi của hàm này
			return;
		}
		if (delta == 0) {
			System.out.println("Phương trình có nghiệm kép: " + (-b / (2 * a)));
		} else {
			System.out.println("Phương trình có hai nghiệm phân biệt: ");
			System.out.println("x1 = " + ((-b + Math.sqrt(delta)) / (2 * a)));
			System.out.println("x2 = " + ((-b - Math.sqrt(delta)) / (2 * a)));
		}
	}

	public static void nhapThongTin() {
		Scanner sc = new Scanner(System.in);
		double a, b, c;

		System.out.println("Nhập số a: ");
		a = sc.nextDouble();
		System.out.println("Nhập số b: ");
		b = sc.nextDouble();
		System.out.println("Nhập số c: ");
		c = sc.nextDouble();

		if (a != 0) {
			giaiPhuongTrinhBacHai(a, b, c);
		} else {
			System.out.println("Đây là phương trình bậc nhất!");
		}
		sc.close();
	}

	public static void main(String[] args) {
		// ax^2 + bx + c = 0

//		PhuongTrinhBacHai phuongTrinhBacHai = new PhuongTrinhBacHai();
//		phuongTrinhBacHai.nhapThongTin();

//		nhapThongTin();
		
		System.out.println(Math.pow(5, (1/5)));
		int year = 10;
		long day = (long) (year * 365.24);
	}
}
