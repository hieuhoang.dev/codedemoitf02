/*
* HieuHT24
*/
package day4;

import java.util.Scanner;

public class Day4Demo {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in); //scanner
		
		System.out.println("Nhập số thứ nhất: ");
		int a = sc.nextInt();
		//Trong trường hợp ở dưới có nhập một chuỗi 
		//sc.nextLine();
		System.out.println("Nhập số thứ hai: ");
		int b = sc.nextInt();
		
		System.out.println(a - b);
		
		//cách 1:
		System.out.println(Integer.compare(a, b));
		
		//Cách 2:
		String ketQua = "";
		if(a < b) {
			ketQua = "a nhỏ hơn b";
		}
		else {
			if(a > b) {
				ketQua = "a lớn hơn b";
			}else {
				ketQua = "a bằng b";
			}
		}
		System.out.println(ketQua);
		
		//cách 3 - toán tử 3 ngôi
		String kq = (a < b) ? "a nhỏ hơn b" : (a == b) ? "a bằng b" : "a lớn hơn b";
		System.out.println(kq);
		sc.close();
	}
}
