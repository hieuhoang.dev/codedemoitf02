/*
* HieuHT24
*/
package day2;

import java.util.Scanner;

import day1.Node;

public class QuanLyHocSinh {
	static HocSinh head = null;
	static Scanner sc = new Scanner(System.in);

	public static void themHocSinhVaoDanhSach(String maHS, 
									   String tenHS, 
									   float diemToan, 
									   float diemLy, 
									   float diemHoa, 
									   float diemVan,
									   float diemNgoaiNgu) {
		HocSinh newNode = new HocSinh(maHS, tenHS, diemToan, diemLy, diemHoa, diemVan, diemNgoaiNgu);
		
		if(head == null) {
			head = newNode;
			return;
		}
		
		HocSinh current = head;
		while (current.next != null) {
			current = current.next;
		}
		current.next = newNode;
		
	}
	
	public void nhapHocSinh() {
		System.out.println("Nhập thông tin học sinh: ");
		System.out.println("Nhập mã học sinh: ");
		String maHS = sc.nextLine();
		System.out.println("Nhập tên học sinh: ");
		String tenHs = sc.nextLine();
		System.out.println("Nhập điểm toán: ");
		float diemToan = sc.nextFloat();
		sc.nextLine();
		System.out.println("Nhập điểm lý: ");
		float diemLy = sc.nextFloat();
		sc.nextLine();
		System.out.println("Nhập điểm hóa: ");
		float diemHoa = sc.nextFloat();
		sc.nextLine();
		System.out.println("Nhập điểm văn: ");
		float diemVan = sc.nextFloat();
		sc.nextLine();
		System.out.println("Nhập điểm ngoại ngữ: ");
		float diemNgoaiNgu = sc.nextFloat();
		sc.nextLine();
		themHocSinhVaoDanhSach(maHS, tenHs, diemToan, diemLy, diemHoa, diemVan, diemNgoaiNgu);
	}
	
	public void hienThiDanhSachHocSinh() {
		HocSinh current = head;
		while (current != null) {
			System.out.println("[" + current.maHS + "-" + current.tenHS + "-"
								 + current.diemToan + "-" + current.diemLy + "-"
								 + current.diemHoa + "-" + current.diemVan + "-"
								 + current.diemNgoaiNgu + "]");
			current = current.next;
		}
		System.out.println("null");
	}
	
	public void xoaThongTinHocSinh() {
		
		if(head == null) {
			System.out.println("Không có học sinh trong danh sách");
			return;
		}
		
		System.out.println("Nhập mã học sinh muốn xóa: ");
		String maHsMuonXoa = sc.nextLine();
		
		if(head.maHS.equals(maHsMuonXoa)) {
			head = head.next;
			return;
		}
		
		HocSinh curent = head;
		while (curent.next != null) {
			if(curent.next.maHS.equals(maHsMuonXoa)) {
				break;
			}
			curent = curent.next;
		}
		
		if(curent.next != null) {
			curent.next = curent.next.next;
		}
		
	}

	public static void main(String[] args) {
		QuanLyHocSinh quanLyHocSinh = new QuanLyHocSinh();
		while (true) {
			System.out.println("Hệ thống quản lý học sinh");
			System.out.println("1. Thêm học sinh");
			System.out.println("2. Hiển thị học sinh");
			System.out.println("3. Xóa thông tin học sinh theo mã học sinh");
			System.out.println("0. Thoát");

			System.out.println("Mời bạn lựa chọn: ");
			int luaChon = sc.nextInt();
			sc.nextLine();

			switch (luaChon) {
			case 1:
				quanLyHocSinh.nhapHocSinh();
				break;
			case 2:
				quanLyHocSinh.hienThiDanhSachHocSinh();
				break;
			case 3:
				quanLyHocSinh.xoaThongTinHocSinh();
				break;
			case 0:
				System.out.println("Thoát chương trình!");
				System.exit(0);
				break;
			default:
				break;
			}
		}
	}
}
