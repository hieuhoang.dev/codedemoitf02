/*
* HieuHT24
*/
package day2;

public class HocSinh {
	public String maHS;
	public String tenHS;
	public float diemToan;
	public float diemLy;
	public float diemHoa;
	public float diemVan;
	public float diemNgoaiNgu;
	HocSinh next;
	public HocSinh(String maHS, String tenHS, float diemToan, float diemLy, float diemHoa, float diemVan,
			float diemNgoaiNgu) {
		super();
		this.maHS = maHS;
		this.tenHS = tenHS;
		this.diemToan = diemToan;
		this.diemLy = diemLy;
		this.diemHoa = diemHoa;
		this.diemVan = diemVan;
		this.diemNgoaiNgu = diemNgoaiNgu;
		this.next = null;
	}
	
}
