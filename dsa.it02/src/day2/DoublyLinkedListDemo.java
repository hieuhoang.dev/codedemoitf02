/*
* HieuHT24
*/
package day2;

public class DoublyLinkedListDemo {
	Node head = null;
	
	public void append(int data) {
		Node newNode = new Node(data);
		
		if(head == null) {
			head = newNode;
			return;
		}
		
		Node current = head;
		while (current.next != null) {
			current = current.next;
		}
		
		current.next = newNode;
		newNode.prev = current;
	}
	
	public void display() {
		Node current = head;
		while (current != null) {
			System.out.print(current.data + " <-> ");
			current = current.next;
		}
		System.out.println("null");
	}
	
	//xóa một phần từ theo giá trị
	public void delete(int data) {
		if(head == null) {
			System.out.println("Làm gì có gì mà xóa");
			return;
		}
		
		if(head.data == data) {
			head = head.next;
			return;
		}
		
		Node current = head;
		while (current.next != null) {
			if(current.data == data) {
				break;
			}
			current = current.next;
		}
		
		current.prev.next = current.next;
		current.next.prev = current.prev;
	}
	
	public static void main(String[] args) {
		DoublyLinkedListDemo doublyLinkedListDemo = new DoublyLinkedListDemo();
		System.out.println("Trước khi thêm phần tử");
		doublyLinkedListDemo.display();
		doublyLinkedListDemo.append(10);
		doublyLinkedListDemo.append(20);
		doublyLinkedListDemo.append(30);
		doublyLinkedListDemo.append(40);
		doublyLinkedListDemo.append(50);
		System.out.println("Sau khi thêm phần tử");
		doublyLinkedListDemo.display();
		System.out.println("Sau khi xóa phần tử");
		doublyLinkedListDemo.delete(30);
		doublyLinkedListDemo.display();
	}
}
