/*
* HieuHT24
*/
package day3;

import java.util.LinkedList;

public class CircularLinkedListDemo {
	Node head = null;
	Node tail = null;
	
	public void append(int data) {
		Node newNode = new Node(data);
		
		if(head == null) {
			head = newNode;
			tail = newNode;
			tail.next = head;
			return;
		}
		
		tail.next = newNode;
		tail = newNode;
		tail.next = head;
	}
	
	public void display() {
		if(head == null) {
			System.out.println("Danh sách trống");
			return;
		}
		Node current = head;
		do {
			System.out.print(current.data + " -> ");
			current = current.next;
		} while (current != head);
		System.out.println("(head)");
	}
	
	public static void main(String[] args) {
		CircularLinkedListDemo circularLinkedListDemo = new CircularLinkedListDemo();
		circularLinkedListDemo.display();
		circularLinkedListDemo.append(0);
		circularLinkedListDemo.append(10);
		circularLinkedListDemo.append(20);
		circularLinkedListDemo.append(30);
		circularLinkedListDemo.display();
		
		LinkedList<Student> linkedList = new LinkedList<Student>(); 
		Student student = new Student("Hiếu", 30, 10);
		Student student1 = new Student("Minh", 30, 10);
		linkedList.add(student);
		linkedList.add(student1);
		System.out.println(linkedList.getFirst().name);
		System.out.println(linkedList.getLast().name);
	}
}
