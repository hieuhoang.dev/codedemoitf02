/*
* HieuHT24
*/
package day3;

import java.util.Scanner;

public class StudentManagment {
	static Scanner sc = new Scanner(System.in);
	static Student head = null;
	static int count = 0;

	public static void addStudent(String name, int age, float mark) {
		Student newNode = new Student(name, age, mark);

		if (head == null) {
			head = newNode;
			count++;
			return;
		}
		
		System.out.println("Vị trí bạn muốn thêm: ");
		System.out.println("1 - Thêm vào vị trí đầu");
		System.out.println("2 - Thêm vào vị trí bất kỳ ở giữa");
		System.out.println("3 - Thêm vào vị trí cuối");
		System.out.println("Chọn vị trí mà bạn muốn thêm: ");
		int choice = sc.nextInt();
		sc.nextLine();
		Student current = head;
		switch (choice) {
		case 1:
			newNode.next = head;
			head = newNode;
			count++;
			break;
		case 2:
			System.out.println("Nhập vị trí mà bạn muốn thêm: ");
			int index = sc.nextInt();
			sc.nextLine();
			int temp = 0;
			current = head;
			while (current != null) {
				temp++;
				if(temp == index) {
					newNode.next = current.next;
					current.next = newNode;
					count++;
				}
				current = current.next;
			}
			if(temp < index) {
				System.out.println("Không có vị trí mà bạn muốn thêm");
			}
			break;
		case 3:
			current = head;
			while (current.next != null) {
				current = current.next;
			}
			current.next = newNode;
			count++;
			break;
		default:
			System.out.println("Bạn chỉ được nhập từ 1 đến 3");
			break;
		}
	}

	public void inputInfoStudent() {
		System.out.println("Nhập thông tin sinh viên: ");
		System.out.println("Nhập tên sinh viên: ");
		String name = sc.nextLine();
		System.out.println("Nhập tuổi sinh viên: ");
		int age = sc.nextInt();
		sc.nextLine();
		System.out.println("Nhập điểm của sinh viên");
		float mark = sc.nextFloat();
		sc.nextLine();
		addStudent(name, age, mark);
	}

	public void display() {
//		if (count < 6) {
//			System.out.println("Nhập chưa đủ số lượng 6 sinh viên");
//			return;
//		}
		Student current = head;
		while (current != null) {
			System.out.println(current.name + "-" + current.age + "-" + current.mark);
			current = current.next;
		}
	}

	public static void main(String[] args) {
		StudentManagment studentManagment = new StudentManagment();
		while (true) {
			System.out.println("Hệ thống quản lý sinh viên");
			System.out.println("1 - Thêm sinh viên");
			System.out.println("2 - Danh sách sinh viên");
			System.out.println("0 - Thoát");

			System.out.println("Mời bạn nhập lựa chọn");
			int choice = sc.nextInt();
			sc.nextLine();
			switch (choice) {
			case 1:
				studentManagment.inputInfoStudent();
				break;
			case 2:
				studentManagment.display();
				break;
			case 0:
				System.out.println("Kết thúc chương trình");
				System.exit(0);
				break;
			default:
				break;
			}
		}
	}
}
