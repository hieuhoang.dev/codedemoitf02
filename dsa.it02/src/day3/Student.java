/*
* HieuHT24
*/
package day3;

public class Student {
	public String name;
	public int age;
	public float mark;
	Student next;
	
	public Student(String name, int age, float mark) {
		this.name = name;
		this.age = age;
		this.mark = mark;
		this.next = null;
	}
	
	
}
