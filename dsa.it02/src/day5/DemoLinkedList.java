/*
* HieuHT24
*/
package day5;

public class DemoLinkedList {
	static Node head = null;
	
	public static void append(int data) {
		Node newNode = new Node(data);
		
		if(head == null) {
			head = newNode;
			return;
		}
		
		Node current = head;
		while (current.next != null) {
			current = current.next;
		}
		
		current.next = newNode;
	}
	
	public static void fun1(Node head) {
		if (head == null)
			return;

		fun1(head.next);
		System.out.print("  " + head.data);
	}
	
	public static void main(String[] args) {
		DemoLinkedList.fun1(head);
		DemoLinkedList.append(10);
		DemoLinkedList.append(20);
		DemoLinkedList.append(30);
		DemoLinkedList.append(40);
		DemoLinkedList.append(50);
		DemoLinkedList.fun1(head);
		
	}
			
}
