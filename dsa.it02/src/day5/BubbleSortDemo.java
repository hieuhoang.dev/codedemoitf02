/*
* HieuHT24
*/
package day5;

import java.util.Arrays;

public class BubbleSortDemo {
	
	public static void bubbleSort(int[] array) {
		int temp;
		//boolean isSwap = false;
		
		//Lặp tất cả các số
		for (int i = 0; i < array.length; i++) {
			//isSwap = false;
			for (int j = 0; j < array.length - i - 1; j++) {
				//Kiểm tra xem giá trị tại vị trí hiện tại và
				//vị trí tiếp theo có bằng nhau không
				//nếu bằng nhau thì hoán đổi
				if(array[j] > array[j+1]) {
					temp = array[j];
					array[j] = array[j + 1];
					array[j + 1] = temp;
					//isSwap = true;
				}
			}
			
			//Nếu mảng đã được sắp xếp rồi thì thoát khỏi vòng lặp
//			if(isSwap == false) {
//				break;
//			}
		}
		System.out.println(Arrays.toString(array));
	}
	
	public static void main(String[] args) {
		int[] array = { 6, 5, 7, 0, 4, 1, 3, 9 };
		
		System.out.println("In ra trước khi sắp xếp");
		System.out.println(Arrays.toString(array));
		System.out.println("In ra sau khi sắp xếp");
		bubbleSort(array);
		
	}
}
