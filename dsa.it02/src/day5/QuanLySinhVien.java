/*
* HieuHT24
*/
package day5;

import java.util.Scanner;

public class QuanLySinhVien {
	static Scanner sc = new Scanner(System.in);
	static SinhVien[] danhSachSinhVien;

	public static void nhapThongTinSinhVien() {
		System.out.println("Nhập số lượng sinh viên: ");
		int soLuong = sc.nextInt();
		sc.nextLine();

		danhSachSinhVien = new SinhVien[soLuong];

		for (int i = 0; i < danhSachSinhVien.length; i++) {
			System.out.println("Nhập sinh thứ " + (i + 1));
			SinhVien sinhVien = new SinhVien();
			System.out.println("Nhập tên sinh viên: ");
			sinhVien.ten = sc.nextLine();
			System.out.println("Nhập tuổi sinh viên: ");
			sinhVien.tuoi = sc.nextInt();
			sc.nextLine();
			System.out.println("Nhập địa chỉ sinh viên: ");
			sinhVien.diaChi = sc.nextLine();
			danhSachSinhVien[i] = sinhVien;
		}
	}

	public static void hienThiDanhSachSinhVien() {
		for (SinhVien sinhVien : danhSachSinhVien) {
			System.out.println(sinhVien.ten + " - " + sinhVien.tuoi + " - " + sinhVien.diaChi);
		}
	}
	
	public static void sapXepTheoTuoi() {
		SinhVien tam;
		for (int i = 0; i < danhSachSinhVien.length; i++) {
			tam =  null;
			for (int j = 0; j < danhSachSinhVien.length - i - 1; j++) {
				if(danhSachSinhVien[j].tuoi > danhSachSinhVien[j+1].tuoi) {
					tam = danhSachSinhVien[j];
					danhSachSinhVien[j] = danhSachSinhVien[j + 1];
					danhSachSinhVien[j + 1] = tam;
				}
			}
		}
		
	}

	public static void main(String[] args) {
		while (true) {
			System.out.println("Hệ thống quản lý sinh viên");
			System.out.println("1 - Nhập thông tin sinh viên");
			System.out.println("2 - Sắp xếp danh sách sinh viên");
			System.out.println("3 - Hiển thị danh sách sinh viên");
			System.out.println("0 - Thoát");

			System.out.println("Mời bạn nhập lựa chọn: ");
			int chon = sc.nextInt();
			sc.nextLine();

			switch (chon) {
			case 1:
				QuanLySinhVien.nhapThongTinSinhVien();
				break;
			case 2:
				QuanLySinhVien.sapXepTheoTuoi();
				break;
			case 3:
				QuanLySinhVien.hienThiDanhSachSinhVien();
				break;
			case 0:
				System.out.println("Kết thúc chương trình");
				System.exit(0);
				break;
			default:
				break;
			}
		}
	}
}
