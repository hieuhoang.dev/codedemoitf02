/*
* HieuHT24
*/
package day4;

public class QueueDemo {
	private int[] queueArray;
	private int maxSize;
	private int top;
	private int front;
	
	public QueueDemo(int size) {
		//Khởi tạo mảng lưu trữ queue
		queueArray = new int[size];
		
		//Chỉ số của phần tử ở đầu hàng đợi, 
		//Lúc đầu chưa có front = 0
		front = 0;
		
		//top là chỉ số để thêm vào cuối hàng đợi
		top = -1;
		
		//số lượng phần tử hiện có trong queue
		maxSize = 0;
	}
	
	public boolean isFull() {
		return maxSize == queueArray.length;
	}
	
	public boolean isEmpty() {
		return maxSize == 0;
	}
	
	public void enqueue(int value) {
		if(isFull() == false) {
			top = (top + 1) % queueArray.length;
			queueArray[top] = value;
			maxSize++;
		}else {
			System.out.println("Queue đã đầy");
		}
	}
	
	public int dequeue() {
		if(isEmpty() == false) {
			//lấy giá trị đầu tiên trong queue
			int value = queueArray[front];
			//tìm vị trí đầu sau mỗi lấy ra
			front = (front + 1)% queueArray.length;
			maxSize--;
			return value;
		}else {
			System.out.println("Queue trống không có gì để xóa");
			return -1;
		}
	}
	
	public int peek() {
		if(isEmpty() == false) {
			return queueArray[front];
		}else {
			System.out.println("Queue trống không có phần tử đầu tiên");
			return -1;
		}
	}
	
	public static void main(String[] args) {
		QueueDemo queue = new QueueDemo(5);
		System.out.println("Trước khi thêm phần tử: " );
		System.out.println("top: " + queue.top);
		System.out.println("front: " + queue.front);
		System.out.println("maxSize: " + queue.maxSize);
		System.out.println("Thêm phần tử đầu tiên: ");
		queue.enqueue(10);
		System.out.println("top: " + queue.top);
		System.out.println("front: " + queue.front);
		System.out.println("maxSize: " + queue.maxSize);
		System.out.println("Thêm phần tử thứ 2: ");
		queue.enqueue(20);
		System.out.println("top: " + queue.top);
		System.out.println("front: " + queue.front);
		System.out.println("maxSize: " + queue.maxSize);
		System.out.println("Thêm phần tử thứ 3: ");
		queue.enqueue(30);
		System.out.println("top: " + queue.top);
		System.out.println("front: " + queue.front);
		System.out.println("maxSize: " + queue.maxSize);
		System.out.println("Thêm phần tử thứ 4: ");
		queue.enqueue(40);
		System.out.println("top: " + queue.top);
		System.out.println("front: " + queue.front);
		System.out.println("maxSize: " + queue.maxSize);
		System.out.println("Thêm phần tử thứ 5: ");
		queue.enqueue(50);
		System.out.println("top: " + queue.top);
		System.out.println("front: " + queue.front);
		System.out.println("maxSize: " + queue.maxSize);
		
		System.out.println("----------");
		System.out.println("Dequeue lần 1");
		queue.dequeue();
		System.out.println("Giá trị dequeue: " + queue.dequeue());
		System.out.println("top: " + queue.top);
		System.out.println("front: " + queue.front);
		System.out.println("maxSize: " + queue.maxSize);
		System.out.println("Enqueue sau dequeue: ");
		queue.enqueue(50);
		System.out.println("top: " + queue.top);
		System.out.println("front: " + queue.front);
		System.out.println("maxSize: " + queue.maxSize);
		
	}
}
