/*
* HieuHT24
*/
package day4;

import java.util.Iterator;

public class StackDemo {
	private int maxSize;
	private int[] stackArray;
	//xác định số lượng phần tử trong stack
	private int top;
	
	//ví dụ cho stack gồm 5 phần tử
	public StackDemo(int size) {
		//Để lưu trữ kích thước của stack
		maxSize = size;
		
		stackArray = new int[maxSize];
		
		top = -1;
	}
	//-1 == 5-1
	//-1 == 4-1
	//-1 == 3 - 1
	//-1 == 2 -1 
	//-1 == 1-1
	//-1 == 0 -1 
	public boolean isFull() {
		return top == maxSize - 1;
	}
	
	public boolean isEmpty() {
		return top == -1;
	}
	
	public void push(int value) {
		if(isFull() == false) {
			stackArray[++top] = value;
		} else {
			System.out.println("Stack đã đầy không thể thêm vào được nữa");
		}
	}
	
	//xóa bỏ phần tử đầu tiên ra khỏi danh sách
	public int pop() {
		if(isEmpty() == true) {
			System.out.println("Stack rỗng không xóa được");
			return -1;
		}
		else {
			return stackArray[top--];
		}
	}
	
	//lấy ra phần tử đầu tiên và không xóa bỏ
	public int peek() {
		if(isEmpty() == true) {
			System.out.println("Stack rỗng không có gì để xem");
			return -1;
		}
		else {
			return stackArray[top];
		}
	}
	public static void main(String[] args) {
		StackDemo stack = new StackDemo(5);
		stack.push(10);
		stack.push(20);
		stack.push(30);
		stack.push(40);
		stack.push(50);
		stack.push(60);
		//lấy ra danh sách các phần tử
//		for (int i = ; i < args.length; i++) {
//			
//		}
		System.out.println("Peek: " + stack.peek());
		while (stack.isEmpty() == false) {
			System.out.println("Pop: " + stack.pop());
		}
		
		System.out.println("Peek: " + stack.peek());
		//System.out.println(stack.stackArray[4]);
	}
}
