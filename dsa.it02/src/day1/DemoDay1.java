/*
* HieuHT24
*/
package day1;

import java.util.Arrays;

public class DemoDay1 {
	public static void main(String[] args) {
		int[] arr = { 4, 5, 1, 2, 3 };
		
		Arrays.sort(arr);
		
		System.out.println(Arrays.toString(arr));
	}
}
