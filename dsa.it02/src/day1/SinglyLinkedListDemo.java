/*
* HieuHT24
*/
package day1;

public class SinglyLinkedListDemo {
	Node head = null;
	
	public void append(String name, int age) {
		//B1 nạp thông tin vào Node
		Node newNode = new Node(name, age);
		 
		//B2 kiểm tra xem danh sách có trống
		if(head == null) {
			//gán newNode vào head
			head = newNode;
			//vì mỗi lần chỉ gán được 1 node nên return
			return;
		}
		
		//B3: nếu không xảy bước b2 thì sẽ sang bước
		//Danh sách đã có phần tử và 
		//chúng ta thêm vào cuối danh sách
		Node current = head;
		while (current.next != null) {
			current = current.next;
		}
		current.next = newNode;
	}
	
	public void appendWithIndex(int index, String name, int age) {
		Node newNode = new Node(name, age);
		
		if(head == null) {
			head = newNode;
			return;
		}
		
		int count = 0;
		Node current = head;
		while (current != null) {
			count++;
			current = current.next;
		}
		
		if(count >= index) {
			current = head;
			count = 0;
			while (current.next != null) {
				count++;
				if(count == index) {
					newNode.next = current.next;
					current.next = newNode;
				}
				current = current.next;
			}
		}else {
			System.out.println("Không có vị trí muốn thêm vào!!!");
		}
		
	}
	
	public void display() {
		Node current = head;
		while (current != null) {
			System.out.print("[" + current.name + "-" + current.age + "]" + " => ");
			current = current.next;
		}
		System.out.println("null");
	}
	
	public static void main(String[] args) {
		SinglyLinkedListDemo linkedListDemo = new SinglyLinkedListDemo();
		System.out.println("Trước khi thêm node");
		linkedListDemo.display();
		linkedListDemo.append("Hiếu", 30);
		linkedListDemo.append("Cao", 28);
		linkedListDemo.append("Minh", 2);
		System.out.println("Sau khi thêm node");
		linkedListDemo.display();
		System.out.println("Thêm node mới vào vị trí thứ 2");
		linkedListDemo.appendWithIndex(2, "Trâm", 29);
		linkedListDemo.display();
	}
}
