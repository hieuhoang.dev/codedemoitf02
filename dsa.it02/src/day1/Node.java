/*
* HieuHT24
*/
package day1;

public class Node {
	String name;
	int age;
	Node next;
	
	Node(String name, int age){
		this.name = name;
		this.age = age;
		this.next = null;
	}
}
